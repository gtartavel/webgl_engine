# Simple WebGL Engine

This page will give you an overview of this project.
I made it to understand better what is OpenGL and to experiment WebGL, its HTML/JavaScript implementation.


## Engine Demos

You can have a look at several demos:

+ an [interactive](../examples/3d.html) demo,
  showing a rotating 3D mesh you can customize.
+ a [video](../examples/video.html) demo,
  playing a video on a mesh, with motion-handling on mobile devices.
+ a [webcam](../examples/webcam.html) demo,
  displaying the webcam content on a mesh, and handling mouse or touch events.


## Renderer Description

This sections give an overview of the engine's structure.
It provides a summary of the content of each class.
It shows in particular how objects are nested, and list their main properties and methods.

    {@link Renderer}                      ==== the base class of the engine
     -  {@link Renderer#canvas canvas} [read]              --- the canvas to render in
     -  {@link Renderer#draw draw()}                     --- draw an image
     -  {@link Renderer#launch launch()}                   --- launch the rendering in a loop
     +- {@link Renderer#camera camera} : {@link Camera}            === the camera to render from
     |   -  {@link Camera set..(..)}               -- set the proporties of the camera
     +- {@link Renderer#sprites sprites[]} : {@link Sprite}         === 2D textures displayed on the screen
     |   -  {@link Sprite#alpha alpha}                   -- transparency
     |   -  {@link Sprite#setPosition setPosition(..)}         -- set the position of the sprite
     |   +- {@link Sprite#texture texture} : {@link Texture}       == texture to be displayed
     |   |   -  {@link Texture#setFlags setFlags(..)}         - set some properties of the texture
     |   |   -  {@link Texture set..(..)}            - set the content of the texture
     |   |   -  {@link Texture load..(..)}           - load the texture, from an image, a
     +- {@link Renderer#scene scene} : {@link Scene}              === a scene to be rendered
     |   |  {@link Scene#matrix matrix}                  -- transformation of the scene
     |   +- {@link Scene#lights lights[]} : {@link Light}        == lights of the scene
     |   |   -  {@link Light#color color}                - color and intensity of the light
     |   |   -  {@link Light#color flags}                - some properties of the light
     |   |   -  {@link Light#position position}             - position of the light, for point or spot lights
     |   |   -  {@link Light#direction direction}            - direction of the light, for directional or spot lights
     |   |   -  {@link Light#direction decay}                - light decay, according to the distance
     |   |   -  {@link Light#direction range}                - light range, according to the radius, if directional or spot
     |   +- {@link Scene#children children[]} : {@link Mesh}|Scene == objects and sub-scenes of the scene
     |   |   -  {@link Mesh#specular specular}             - shininess of the object
     |   |   -  {@link Mesh#flags flags}                - some properties of the object
     |   |   -  {@link Mesh#scale scale}                - scale of the object
     |   |   -  {@link Mesh make..(..)}           - create a mesh
     |   |   -  {@link Mesh load..(..)}           - load a mesh
     |   |   -  {@link Mesh#setData setData(..)}          - set the data arrays of the mesh
     |   |   +  {@link Mesh#texture texture} : {@link Texture}    = texture of the objectvideo, or a webcam
     |   |   |   +  <cf. in {@link Sprite}>    <see a few lines above>


## Files Description

The project contains the following files:

    -  readme.md                    -- this file, i.e. documentation main file
    -  jsduck.json                  -- documentation configuration, just type `jsduck`
    -  categories.json              -- documentation categories
    -  engine-all.js                -- standalone source file, containing all the JS code
    +- src/                         -- source files
    |   -  Makefile                 -- generate the single source file `engine-all.js`
    |   +  <files>.js               -- JavaScript source files
    |   +  <files>.glsl             -- Shader source files.
    +- examples/                    -- some HTML examples
    +- data/                        -- data such as 3D models, textures, etc.
    +- doc/                         -- generated documentation, in HTML


## Suggestions

### Todo List

+ Deal with **Mesh** color without creating a texture
+ Add more options to the **demo**, including:
    + load a new mesh
    + change color and texture
    + wireframe and point cloud rendering


### Known Bugs

+ Video **Texture** not playing on android
+ Warning with **Texture** on Firefox: `.. clamp .. power of 2 .. black`
