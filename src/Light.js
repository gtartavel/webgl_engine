/*jslint browser: true, vars: true, plusplus: true, bitwise: true */
/*global vec3 */
/*exported Light */


/** A 3D light.
 *
 *  This class deals with:
 *
 *  + Different kind of lights: directional, point, sport.
 *  + Different lighting: diffuse, specular.
 *
 * @constructor
 *  Create a Light.
 *
 * @param {Number | vec3 | vec4} [color = 1]
 *  Color and/or intensity of the light.
 * @param {vec3} [position = null]
 *  Position of the light.
 * @param {vec3} [direction = null]
 *  Direction of the light.
 * @param {Number} [flags = 0]
 *  Flags, combination of: `Light.NODIFFUSE, Light.NOSPECULAR, Light.POINT`.
 *  Default is `POINT` if `direction` is set, `NODIFFUSE` otherwise.
 * @param {vec2} [decay = null]
 *  Decay of the light.
 *  First value is the distance of reference, second one is the exponent.
 * @param {vec3} [range = null]
 *  Radial range of the light.
 *  First and second values are the radius of light and penumbra.
 *  Third one is the exponent.
 */
function Light(color, position, direction, flags, decay, range) {
    'use strict';
    /** Position of the Light, in the parent Scene. @type {vec3} */
    this.position = position || [0, 0, 0];
    /** Direction of the light, not necessarily normalized. @type {vec3} */
    this.direction = direction || [0, -1, 0];
    /** Light properties. @type {Number} */
    this.flags = flags || 0;
    /** Color (RGB) and intensity of the light. @type {vec4} */
    this.color = (color && color.length) ? color : [color || 0];

    /** Light decay over distance.
     *
     * Attenuation is: `(decay[0] / dist) ^ decay[1]`. @type {vec2} */
    this.decay = decay || [1, 0];
    /** Range of the light, i.e. radial decay. @type {vec3}
     *
     * On a plane at distance `1`, the light decays from circles of radius `range[0]` to `range[1]`
     * with a transition of exponent `range[2]`. */
    this.range = range || [999, 1000, 0];

    /** @protected Scene containing the light. @type {Scene} */
    this.parent = null;
    /** @protected Lighted Scene. @type {Scene} */
    this.child = null;

    // Initialize
    if (flags === undefined || flags === null) {
        this.flags = position ? Light.POINT : Light.NOSPECULAR;
    }
    switch (this.color.length) {
    case 1:
        this.color = [1, 1, 1, this.color[0]];
        break;
    case 3:
        this.color[3] = 1;
        break;
    case 4:
        break;
    default:
        throw new Error('Invalid light color');
    }

    // Buffers
    this.dirView = vec3.create();
    this.posView = vec3.create();

    // End of the constructor
    return this;
}


/* Settings */          /** Maximum number of lights. @cfg @type {Number} */
Light.MAX = 4;          /** Flag: no diffused light. @cfg @type {Number} */
Light.NODIFFUSE = 1;    /** Flag: no specular light. @cfg @type {Number} */
Light.NOSPECULAR = 2;   /** Flag: point/spot light, instead of directional. @cfg @type {Number} */
Light.POINT = 4;


/** Set or change the lighted Scene.
 *
 * @param {Scene} parent
 *  Scene containing the Light.
 * @param {Scene} [child = parent]
 *  Lighted scene.
 */
Light.prototype.toScene = function (parent, child) {
    'use strict';
    this.parent = parent;
    if (this.child) {
        var k = this.child.lights.indexOf(this);
        if (k >= 0) {
            this.child.lights.splice(k, 1);
        }
    }
    this.child = child || parent;
    this.child.lights.push(this);
    return this;
};

/** Overload this function to define the Light animation.
 *
 * This function is called once per frame.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Light.prototype.animate = function () {
    'use strict';
    return;
};

/** Export the light properties to uniform variables of the shader. @protected
 *
 * @param {Number} n
 *  Index of the light in the shader.
 */
Light.prototype.toUniform = function (n) {
    'use strict';
    if (n >= Light.MAXLIGHT) {
        throw new Error('Too many lights');
    }
    var R = this.parent.R;
    var gl = R.ctx;
    var c = this.color;
    var d = vec3.transformMat3(this.dirView, this.direction, this.parent.matNormals);
    var p = vec3.transformMat4(this.posView, this.position, this.parent.matView);
    var r = this.range;
    var f = [this.flags & Light.NODIFFUSE, this.flags & Light.NOSPECULAR, this.flags & Light.POINT];
    gl.uniform4i(R.loc.uLitFlags[n], !f[0], !f[1], f[2], false);
    gl.uniform3f(R.loc.uLitColors[n], c[0] * c[3], c[1] * c[3], c[2] * c[3]);
    gl.uniform4f(R.loc.uLitPositions[n], p[0], p[1], p[2], this.decay[0]);
    gl.uniform4f(R.loc.uLitDirections[n], d[0], d[1], d[2], this.decay[1]);
    gl.uniform3f(R.loc.uLitRanges[n], r[0], r[1], r[2]);
};

/** Remove the light from the uniform variables. @static @protected
 *
 * @param {Renderer} R
 * @param {Number} n
 *  Index of the light in the shader.
 */
Light.unsetUniform = function (R, n) {
    'use strict';
    R.ctx.uniform4i(R.loc.uLitFlags[n], false, false, false, false);
};
