/*jslint browser: true, vars: true, plusplus: true, bitwise: true */
/*global Uint8Array, MediaStreamTrack */
/*exported Texture */

/** A Texture.
 *
 * This class deals with:
 *
 *  + texture loading and settings.
 *  + texture from video or webcam.
 *
 * @constructor
 *  Create a new Texture.
 *
 * @param {Renderer} parent
 *  Parent Renderer.
 * @param {Number | Array | String} [data = 0]
 *  Texture data/content, can be:
 *
 *  + a gray value in 0..1.
 *  + a gray, RGB, or RGBA array of values.
 *  + the URL of an image.
 */
function Texture(parent, data, altColor) {
    'use strict';
    var gl = parent.ctx;
    /** @protected @type {Renderer} */
    this.R = parent;
    /** @protected Texture's properties. type {Number} */
    this.flags = 0;
    /** Canvas to update the texture from. */
    this.srcCanvas = null;

    /** @private The GPU texture. */
    this.texture = gl.createTexture();
    this.setFlags(0);
    if (typeof data === 'string') {
        this.setColor((altColor !== undefined && altColor !== null) ? altColor : 0);
        this.loadImage(data);
    } else {
        this.setColor(data || 0);
    }
    return this;
}

/* Settings */                  /** Flag: don't repeat the texture. @cfg @type {Number} */
Texture.CLAMPED = 1;            /** Flag: interpolate when zooming in. @cfg @type {Number} */
Texture.LINEAR_IN = 2;          /** Flag: interpolate when zooming out. @cfg @type {Number} */
Texture.LINEAR_OUT = 4;         /** Flag: use mipmap (precomputed smaller textures) when zooming out. @cfg @type {Number} */
Texture.MIPMAP = 8;             /** Flag: interpolate between mipmap when zooming out. @cfg @type {Number} */
Texture.LINEAR_MIPMAP = 16;     /** Flags: interpolate when zooming. @protected @cfg @type {Number} */
Texture.BILINEAR = Texture.LINEAR_IN & Texture.LINEAR_OUT;      /** Flags: interpolate when zooming and interpolate mipmap. @cfg @type {Number} */
Texture.TRILINEAR = Texture.BILINEAR & Texture.MIPMAP & Texture.LINEAR_MIPMAP;

/** Overload this function to define the Texture animation.
 *
 * This function is called once per frame.
 * The default function calls the {@link Texture#setImage} to load the texture from the {@link Texture#srcCanvas} if any.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Texture.prototype.animate = function () {
    'use strict';
    if (this.srcCanvas) {
        this.setImage(this.srcCanvas, true, Texture.BILINEAR);
    }
    return;
};

/** Export the texture properties to uniform variables of the Shader. @protected
 *
 * @param {Number} location
 *  Uniform texture (sampler2D) location.
 * @param {Number} slot
 *  Slot to export the texture to.
 */
Texture.prototype.toUniform = function (location, slot) {
    'use strict';
    slot = slot || 0;
    if (slot >= 32) {
        throw new Error('Invalid texture slot');
    }
    var gl = this.R.ctx;
    gl.activeTexture(gl['TEXTURE' + slot]);
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.uniform1i(location, slot);
};

/** Test whether an integer is a power of 2. @static */
Texture.isPowerOfTwo = function(n) {
    'use strict';
    var b = n & (n - 1);
    return (n && !b);
};


/* ***************  SET & LOAD METHODS  *************** */

/** Load the Texture from an Image.
 *
 * @param {String} imUrl
 *  Url of the image.
 * @param {Function} [callback]
 *  Callback function, called with the Texture as `this`.
 * @param {Number} [flags]
 *  Flags of the Texture, default is `BILINEAR & MIPMAP` for power of 2.
 */
Texture.prototype.loadImage = function(imUrl, callback, flags) {
    'use strict';
    var image = new Image();
    var that = this;
    if (flags === undefined || flags === null) {
        flags = Texture.BILINEAR & Texture.MIPMAP;
    }

     // Callback functions
    image.onload = function () {
        that.clearStream();
        that.setImage(image, true, flags);
        if (callback) {
            callback.call(that);
        }
    };
    image.onerror = function() {
        throw new Error('Image cannot be loaded: ' + imUrl);
    };

    // Load and return
    image.src = imUrl;
    return this;
};

/** Load (and update) the Texture from a Video.
 *
 * @param {String} videoUrl
 *  URL of the video.
 * @param {Number} [volume = null]
 *  Volume of the sound, muted if not specified.
 * @param {Function} [startFcn]
 *  Function called before the video is launched.
 *  The function is called with the Texture as `this`.
 * @param {Function | Boolean} [endFcn = false]
 *  Function called when the video is ended.
 *  A boolean value tells whether the video must be played once or looped.
 */
Texture.prototype.loadVideo = function (videoUrl, volume, startFcn, endFcn) {
    'use strict';
    var that = this;

    // Create the element
    var videoElmt = window.document.createElement('video');
    videoElmt.style.display = 'none';
    videoElmt.preload = 'auto';
    videoElmt.autoplay = true;
    videoElmt.volume = volume || 0;
    if (typeof volume !== 'number') {
        videoElmt.muted = true;
    }
    if (!endFcn) {
        videoElmt.loop = true;
    }

    // Define the callback
    var started = false;
    videoElmt.addEventListener("canplaythrough", function () {
        if (!started) {
            started = true;
            that.clearStream();
            that.srcCanvas = videoElmt;
            videoElmt.width = videoElmt.videoWidth;
            videoElmt.height = videoElmt.videoHeight;
            if (startFcn) {
                startFcn.call(that);
            }
            videoElmt.play();
        }
    }, true);
    if (endFcn) {
        videoElmt.addEventListener("ended", function () {
            if (typeof endFcn === 'function') {
                endFcn.call(that);
            }
            that.srcCanvas = null;
        }, true);
    }

    // Start
    window.document.body.appendChild(videoElmt);
    videoElmt.src = videoUrl;
    return this;
};

/** Load (and update) the Texture from the Webcam.
 *
 * @param {Number} [which = 0]
 *  Webcam number.
 *  First is 0, last is -1.
 * @param {Number} [volume = null]
 *  Volume of the microphone, not recorded if not specified.
 * @param {Function} [startFcn]
 *  Function called when the Webcam is started.
 *  The function is called with the Texture as `this`.
 */
Texture.prototype.loadWebcam = function (which, volume, startFcn, webcamSrc) {
    'use strict';
    var that = this;

    // Select the right Camera
    if (which && MediaStreamTrack && MediaStreamTrack.getSources) {
        MediaStreamTrack.getSources(function (srcs) {
            var k, list = [];
            for (k = 0; k < srcs.length; k++) {
                if (srcs[k].kind === 'video') {
                    list.push(srcs[k].id);
                }
            }
            if (which < 0) {
                which += list.length;
            }
            which = Math.min(Math.max(which, 0), list.length - 1);
            webcamSrc = {'optional': [{'sourceId': list[which]}]};
            that.loadWebcam(null, volume, startFcn, webcamSrc);
        });
        return this;
    }

    // Create the element
    var videoElmt = window.document.createElement('video');
    videoElmt.style.display = 'none';
    videoElmt.autoplay = true;
    videoElmt.volume = volume || 0;

    // Create the stream
    navigator.getMedia = navigator.getUserMedia
        || navigator.webkitGetUserMedia
        || navigator.mozGetUserMedia
        || navigator.msGetUserMedia;
    var mediaSrc = {
        'video': webcamSrc || true,
        'audio': (typeof volume === 'number')
    };
    var launchStream = function (stream) {
        if (navigator.mozGetUserMedia) {
            videoElmt.mozSrcObject = stream;
        } else {
            videoElmt.src = (window.URL || window.webkitURL).createObjectURL(stream);
        }
        videoElmt.play();
    };
    navigator.getMedia(mediaSrc, launchStream, function (e) {
        throw new Error("Cannot initialize webcam: " + e);
    });

    // Initialize the video
    var initialized = false;
    videoElmt.addEventListener('canplay', function () {
        if (!initialized) {
            initialized = true;
            that.clearStream();
            that.srcCanvas = videoElmt;
            videoElmt.width = videoElmt.videoWidth;
            videoElmt.height = videoElmt.videoHeight;
            if (startFcn) {
                startFcn.call(that);
            }
        }
    }, true);

    // Start
    window.document.body.appendChild(videoElmt);
    return this;
};

/** If Video/Webcam, remove it and turn the texture to black. */
Texture.prototype.clearStream = function () {
    'use strict';
    if (this.srcCanvas) {
        window.document.body.removeChild(this.srcCanvas);
        this.srcCanvas = null;
        this.setColor(0);
    }
    return this;
};

/** Set the Texture from an Image or a Canvas.
 *
 * @param im
 *  Image or Canvas to get the Texture content from.
 * @param {Boolean} yflip
 *  If true, the image is flipped.
 *  This is useful to change origin from top-left (Image) to bottom-left (Canvas).
 * @param {Number} [flags]
 *  Flags of the Texture, default is: `BILINEAR` in general.
 *  Turned to `CLAMPED` for non-power-of-two dimensions.
 */
Texture.prototype.setImage = function (im, yflip, flags) {
    'use strict';
    if (flags === undefined || flags === null) {
        flags = Texture.BILINEAR;
    }
    var isPowerOfTwo = im && Texture.isPowerOfTwo(im.width) && Texture.isPowerOfTwo(im.height);
    if (!im || !isPowerOfTwo) {
        flags = Texture.CLAMPED;
    }
    this.setFlags(flags, null, true);
    var gl = this.R.ctx;
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, Boolean(yflip));
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, im);
    gl.bindTexture(gl.TEXTURE_2D, null);
    return this;
};

/** Set the Texture as a Text.
 *
 *    // A red text of 20px in a box of width 200px with vertical padding of 2px:
 *    texture.setText('Warning', {'color': 'red'}, 200, -2);
 *
 * @param {String} text
 * @param {Object} [cssProperties]
 *  Properties of the text, whose properties can be set:
 *  `fontFamily`, `fontSize`, `color`, `backgroundColor`, `textAlign`, `verticalAlign`.
 * @param {Number} [width]
 *  Width of the texture; a negative number is interpreted as the padding size.
 * @param {Number} [height = width]
 *  Height of the texture; a negative number is interpreted as the padding size.
 */
Texture.prototype.setText = function (text, css, width, height) {
    'use strict';
    var defSize = '220px';
    css = css || {};

    // Width & size, in a DIV
    Texture.textDiv = Texture.textDiv || window.document.createElement('div');
    var div = Texture.textDiv;
    div.innerText = text;
    div.style.whiteSpace = 'pre';
    div.style.position = 'fixed';
    div.style.visibility = 'hidden';
    div.style.left = div.style.top = '0px';
    div.style.width = div.style.height = 'auto';
    div.style.fontFamily = css.fontFamily || 'times';
    div.style.fontSize = css.fontSize || defSize;
    window.document.body.appendChild(div);
    var divWidth = div.clientWidth;
    var divHeight = div.clientHeight;
    window.document.body.removeChild(div);

    // Get the canvas
    Texture.textCanvas = Texture.textCanvas || window.document.createElement('canvas');
    var canvas = Texture.textCanvas;
    height = (height !== undefined && height !== null) ? height : width;
    canvas.width = (width && width > 0) ? width : divWidth - 2 * (width || 0);
    canvas.height = (height && height > 0) ? height : divHeight - 2 * (height || 0);

    // Configure the text
    var ctx = Texture.textCanvas.getContext('2d');
    ctx.font = (css.fontSize || defSize) + ' ' + (css.fontFamily || 'times');
    ctx.fillStyle = css.color || '#ffffff';
    ctx.textAlign = css.textAlign || 'center';
    ctx.textBaseline = css.verticalAlign || 'middle';

    // Draw the text
    ctx.fillText(text, canvas.width / 2, canvas.height / 2);
    this.setImage(canvas, true);
    return this;
};

/** Set the Texture as a color.
 *
 * @param {Number | Array} [color = 1]
 *  Color made of 1 (gray), 3 (RGB), or 4 (RGBA) values  in range 0..1.
 */
Texture.prototype.setColor = function (color) {
    'use strict';
    var gl = this.R.ctx;

    // Deal with arguments
    color = (color !== undefined && color !== null) ? color : 1;
    if (!color.length) {
        color = [color];
    }

    // Get color
    switch (color.length) {
    case 1:
        color = [color[0], color[0], color[0], 1];
        break;
    case 3:
        color[3] = 1;
        break;
    case 4:
        break;
    default:
        throw new Error('Invalid texture color');
    }

    // Scale to 0..255
    color[0] *= 255;
    color[1] *= 255;
    color[2] *= 255;
    color[3] *= 255;

    // Set the color
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
        new Uint8Array(color));
    gl.bindTexture(gl.TEXTURE_2D, null);

    // Remove useless flags
    this.setFlags(0, Texture.BILINEAR & Texture.MIPMAP);
    return this;
};

/** Set the flags.
 *
 * With one argument: set the new flags.<br/>
 * With two arguments: turn ON/OFF only selected flags.
 *
 * Flags are: `CLAMPED`, `LINEAR_IN`, `LINEAR_OUT`, `MIPMAP`, `LINEAR_MIPMAP`.<br/>
 * Shortcuts are `BILINEAR` (linear IN and OUT), `TRILINEAR` (bilinear with linear mipmap).
 *
 *     // Set bilinear to ON and everything else to OFF:
 *     tex.setFlags(Texture.BILINEAR);
 *
 *     // Set MIPMAP to ON, LINEAR_MIPMAP to OFF, and left others UNCHANGED:
 *     text.setFlags(Texture.MIPMAP, Texture.LINEAR_MIPMAP);
 *
 * @param {Number} flags
 *  New flags values, or flags to be turned ON (if `notFlags` specified).
 * @param {Number} [notFlags]
 *  Flags to be turned OFF.
 */
Texture.prototype.setFlags = function (flags, notFlags, forceMipmap) {
    'use strict';
    var oldFlags = this.flags;
    if (notFlags === undefined || notFlags === null) {
        this.flags = flags || 0;
    } else {
        this.flags |= flags | notFlags;     // turn ON
        this.flags ^= notFlags;             // turn OFF
    }
    var updateMipmap = (oldFlags ^ this.flags) & (this.flags & Texture.MIPMAP);
    this.updateFlags(updateMipmap || forceMipmap);
    return this;
};


/* ***************  PROTECTED METHODS  *************** */

/** Update the properties according to the flags. @private */
Texture.prototype.updateFlags = function (updateMipmap) {
    'use strict';
    var gl = this.R.ctx;
    gl.bindTexture(gl.TEXTURE_2D, this.texture);

    // Zoom
    var filterIn = (this.flags & Texture.LINEAR_IN) ? 'LINEAR' : 'NEAREST';
    var filterOut = (this.flags & Texture.LINEAR_OUT) ? 'LINEAR' : 'NEAREST';
    if (this.flags & Texture.MIPMAP) {
        filterOut += '_MIPMAP_' + (this.flags & Texture.LINEAR_MIPMAP) ? 'LINEAR' : 'NEAREST';
    }
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl[filterIn]);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl[filterOut]);
    if (updateMipmap && (this.flags & Texture.MIPMAP)) {
        gl.generateMipmap(gl.TEXTURE_2D);
    }

    // Repeat
    var repeat = (this.flags & Texture.CLAMPED) ? gl.CLAMP_TO_EDGE : gl.REPEAT;
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, repeat);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, repeat);

    gl.bindTexture(gl.TEXTURE_2D, null);
    return this;
};
