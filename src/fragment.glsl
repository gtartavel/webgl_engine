precision mediump float;

varying vec4 vPosition;
varying vec3 vNormal;
varying vec2 vTexCoord;


// Object color

uniform vec4 uObjColor;
uniform vec2 uObjSpecular;      // Specular is (value, exponent)
uniform bvec4 uObjFlags;        // Flags are (diff, spec, point, -)

uniform sampler2D uSamplerColor;

vec3 computeColor(vec3 point, vec3 normal) {
    vec3 color = uObjColor.rgb;
    if (uObjColor.a > 0.0) {
        vec4 texColor = texture2D(uSamplerColor, vTexCoord);
        color *= texColor.rgb;
    }
    return color;
}

// Real lighting

#define LIGHT_MAX_GAIN  10.0
#define LIGHT_MAX       4       // js: Light.MAX;

uniform vec3 uLitAmbient;
uniform vec3 uLitColors[LIGHT_MAX];
uniform vec4 uLitPositions[LIGHT_MAX];  // Position is (x, y, z, dref)
uniform vec4 uLitDirections[LIGHT_MAX]; // Direciton is (dx, dy, dz, decay)
uniform vec3 uLitRanges[LIGHT_MAX];     // Range is (zfull, znone, decay)
uniform bvec4 uLitFlags[LIGHT_MAX];     // Flags are (diff, spec, point, -)

vec3 computeLighting(vec3 point, vec3 normal) {
    vec3 light = uLitAmbient;
    vec3 ray;           // direction of the light ray
    float dist, rad;    // distance from point to light / cone

    // Loop over useful light
    if (any(uObjFlags.xy))
     for (int k = 0; k < LIGHT_MAX; k++)
      if (any(uLitFlags[k].xy)) {

        // Get light ray
        vec3 dir = normalize(uLitDirections[k].xyz);
        vec3 light2pt = point - uLitPositions[k].xyz;
        if (!uLitFlags[k].z) {  // directional light
            ray = dir;
            dist = dot(light2pt, ray);
            rad = distance(light2pt, dist * ray);           // = dist(pt, axis)
        } else {  // point light
            ray = normalize(light2pt);
            dist = length(light2pt);
            rad = length(cross(dir, ray)) / dot(dir, ray);  // = tan(angle/2)
        }

        // Diffuse and specular
        float cosDiff = -dot(ray, normal);
        float diff = max(0.0, cosDiff) * float(uObjFlags.x && uLitFlags[k].x);
        float spec = 0.0;
        if (uObjFlags.y && uLitFlags[k].y) {
            float cosSpec = reflect(ray, normal).z * step(0.0, cosDiff);
            spec = uObjSpecular.x * pow(max(0.0, cosSpec), uObjSpecular.y);
        }

        // Light decay
        float decr = pow(uLitPositions[k].a / dist, uLitDirections[k].a);
        float border = 1.0;
        if (uLitRanges[k].z != 0.0) {
            float u = (rad - uLitRanges[k].y) / (uLitRanges[k].x - uLitRanges[k].y);
            float expu = pow(clamp(u, 0.0, 1.0), uLitRanges[k].z);
            border = max(expu, step(0.0, -abs(uLitRanges[k].z)));
        }

        // Add contribution
        light += uLitColors[k] * decr * border * (diff + spec);
    }

    // Return the resulting lighting
    return light;
}


// Geometry lighting

const vec3 geoLightA = vec3( 0.6,  1.0,  0.4);
const vec3 geoLightB = vec3(-0.3, -0.2,  1.0);

float geometryLight(vec3 normal) {
    vec3 t = abs(normal);
    return dot(t, geoLightA) + dot(t, geoLightB);
}


// Fog

uniform vec3 uFogRange;     // Fog range is (zmin, zmax, power)
uniform vec3 uFogColor;

void applyFog(inout vec3 color, vec3 position) {
    if (uFogRange.p != 0.0) {
        float d = length(position);    // or d = pos.z
        float t = (d - uFogRange.s) / (uFogRange.t - uFogRange.s);
        color += (uFogColor - color) * pow(clamp(t, 0.0, 1.0), uFogRange.p);
    }
}


// Main Function


void main(void) {
    vec3 normal = normalize(vNormal);
    vec3 position = vPosition.xyz / vPosition.w;

    // Point cloud case: use disks
    if (uObjFlags.z && distance(gl_PointCoord, vec2(.5)) > .5) {
        discard;
    }

    // Compute fragment color
    vec3 color = computeColor(position, normal);
    color *= computeLighting(position, normal);
    applyFog(color, position);

    // Output values
    gl_FragColor = vec4(color, abs(uObjColor.a));
}
