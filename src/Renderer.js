/*jslint browser: true, vars: true, plusplus: true, bitwise: true */
/*global Shader, Scene, Light, Sprite, vec3 */
/*exported Renderer */


/** A basic WebGL 3D renderer.
 *
 *  It provides a simple way of defining and rendering 3D scenes in a HTML page using WebGL.
 *
 *  This class deals with:
 *
 *  + the rendering canvas.
 *  + the WebGL program.
 *  + the program configuration (Camera, Scene, HUD).
 *
 * @constructor
 *  Create a renderer.
 *
 * @param {String | HTMLElement} canvas
 *  The output canvas.
 * @param {String | HTMLElement} [vertexShader]
 *  The vertex shader, same format as the vertex fragment (with type `x-shader/x-vertex`).
 *
 * @param {String | HTMLElement} [fragShader]
 *  The fragment shader, either:
 *
 *  + as source code.
 *  + as a `script` element, or its ID.
 *  + if not specified, loof for a script element of type `x-shader/x-fragment`.
 */
function Renderer(canvas) {
    'use strict';

    /** @protected Background color. @type {vec3} */
    this.color = [0, 0, 0];
    /** @private WebGL context. */
    this.ctx = null;
    /** @private Location of Shader variables. */
    this.loc = {};

    /** @protected Rendering canvas. @type {HTMLElement} */
    this.canvas = null;
    /** Rendering Camera. @type {Camera} */
    this.camera = null;
    /** Rendered Sprites. @type {Sprite[]} */
    this.sprites = [];
    /** Rendered Scene. @type {Scene} */
    this.scene = new Scene(this);

    /** @private WebGL program. */
    this.program = null;
    /** @private Shaders, as field `vertex` and `fragment`. */
    this.shader = {'vertex': null, 'fragment': null};

    /** Set it to `true` to stop the animation. @type{Boolean} */
    this.stop = null;
    /** @protected FPS counter. @type {Number} */
    this.fps = null;

    // Initialize the canvas
    this.canvas = (typeof canvas !== 'string') ? canvas : window.document.getElementById(canvas);
    if (!this.canvas) {
        throw new Error('Invalid canvas');
    }

    // Get the WebGL context
    try {
        this.ctx = this.canvas.getContext('webgl') || this.canvas.getContext('experimental-webgl');
    } catch (e) {
        throw new Error('WebGL initialization failed');
    }

    // Create the shaders
    this.setShaders();

    // Allocate some memory
    this.litSum = vec3.create();

    // Initialize the display
    this.ctx.enable(this.ctx.DEPTH_TEST);
    this.setColor(this.color);

    // Time functions
    var tref = new Date().getTime();
    /** Get the elapsed time since last frame in ms. @protected
     * @return {Number}
     * @param {Boolean} [reset = false] */
    this.toc = function (update) {
        var t = new Date().getTime();
        var dt = t - tref;
        if (update) {
            tref = t;
        }
        return dt;
    };

    // End of the constructor
    return this;
}


/* Settings */                  /** Strength of smoothing for the FPS counter. @cfg @type {Number} */
Renderer.FPS_SMOOTH_MS = 500;   /** Amplitude of FPS variations which mustn't be smoothed. @cfg @type {Number} */
Renderer.FPS_SMOOTH_JUMP = 10;


/** Animate the Camera, the Scene, and the Sprites. @protected
 *
 *  This function is called before rendering each frame. */
Renderer.prototype.animate = function () {
    'use strict';
    var k, dt = this.toc(true);
    this.updateFPS(dt);
    this.camera.animate(dt);
    this.scene.animate(dt);
    for (k = 0; k < this.sprites.length; k++) {
        this.sprites[k].animate(dt);
    }
    return this;
};

/** Draw the scene. */
Renderer.prototype.draw = function () {
    'use strict';
    var k, gl = this.ctx;
    this.animate();
    this.scene.precompute();
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.disable(gl.BLEND);
    this.camera.toUniform(this);
    this.scene.draw(0);
    Sprite.drawInit(this);
    for (k = 0; k < this.sprites.length; k++) {
        this.sprites[k].draw();
    }
};

/** Launch the loop to animate and draw the scene. */
Renderer.prototype.launch = function () {
    'use strict';
    if (this.stop) {
        this.stop = null;
    } else {
        var that = this;
        Renderer.requestAnimationFrame(function () {
            that.launch();
        }, this.canvas);
        this.draw();
    }
};

/** Set the background color.
 *
 * @param {vec3} color
 *  The color, as `[r, g, b]` values in range 0..1.
 */
Renderer.prototype.setColor = function (c) {
    'use strict';
    if (!c || c.length !== 3) {
        throw new Error('Invalid color');
    }
    this.color = c;
    this.ctx.clearColor(c[0], c[1], c[2], 1);
    this.ctx.uniform3f(this.loc.uFogColor, c[0], c[1], c[2]);
    return this;
};


/* ***************  PROTECTED METHODS  *************** */

/** Set the shaders of the program. @protected
 *
 * @param {String | HTMLElement} [vertexShader]
 *  Source code of the vertex Shader, or SCRIPT element (or ID) containing it.
 * @param {String | HTMLElement} [fragShader]
 *  Source code of the fragment Shader, or SCRIPT element (or ID) containing it.
 */
Renderer.prototype.setShaders = function (vertexShader, fragShader) {
    'use strict';
    var gl = this.ctx;

    // Define the shaders
    this.shader.vertex = new Shader(this, false, vertexShader);
    this.shader.fragment = new Shader(this, true, fragShader);

    // Make the program
    this.program = gl.createProgram();
    gl.attachShader(this.program, this.shader.vertex.shader);
    gl.attachShader(this.program, this.shader.fragment.shader);
    gl.linkProgram(this.program);
    if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
        throw new Error('Shader initialization error');
    }
    gl.useProgram(this.program);
    this.loc = this.shader.vertex.extractLocations();
    this.loc = this.shader.fragment.extractLocations(this.loc);

    // The end
    return this;
};

/** Update the number of FPS. @protected
 *
 * Note: FPS is smoothed using `FPS_SMOOTH_MS` as half-decay time.
 *
 * @param {Number} dt
 *  Time spent to compute the last frame, in ms.
 */
Renderer.prototype.updateFPS = function (dt) {
    'use strict';
    var spf = dt / 1000;
    var jmp = Renderer.FPS_SMOOTH_JUMP;
    if ((this.fps && !jmp) || (1 < this.fps * spf * jmp && this.fps * spf < jmp)) {
        var w = Math.exp(-dt / Renderer.FPS_SMOOTH_MS * Math.LN2);
        spf += w * (1 / this.fps - spf);
    }
    this.fps = 1 / spf;
    return this;
};

/** Specify a callback function to be called when next frame is requested. @static @protected
 * @param {Function} callback
 *  Rendering function, which draw the next frame.
 * @param elmt
 *  The canvas in which the animation is played.
 */
Renderer.requestAnimationFrame = (function() {
    'use strict';
    var requestFcn =
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback) {
            window.setTimeout(callback, 1000 / 60);
        };
    var fcn = function(callback) {
        requestFcn.call(window, callback);
    };
    return fcn;
}());
