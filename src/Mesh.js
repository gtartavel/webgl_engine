/*jslint browser: true, vars: true, plusplus: true, bitwise: true, regexp: true */
/*global Scene, Texture, Uint16Array, Float32Array */
/*exported Mesh */


/** A 3D Mesh.
 *
 *  A Mesh is composed by a set of 3D points and a set of triangles to connect them.
 *
 *  This class deals with:
 *
 *  + arrays of Triangles, Vertices, Normals, and texture coordinates.
 *  + their associated GPU buffers.
 *  + mesh creation, loading, and display.
 *
 * @constructor
 *  Create a new Mesh.
 *
 * @param {Scene | Renderer} parent
 *  The scene containing the Mesh.
 * @param {Texture | String | Array | Number} [texture]
 *  Texture, Image URL, or color (gray / RGB / RGBA).
 * @param {vec2} [specular]
 *  Brightness and shininess of the object.
 * @param {Number} [flags]
 *  Flags of the object.
 *  Available: `NODIFFUSE`, `NOSPECULAR`, `NOAMBIENT`, `WIREFRAME`, `POINTCLOUD`.
 */
function Mesh(parent, texture, spec, flags) {
    'use strict';
    /** @protected @type {Scene} */
    this.parent = parent;
    /** @protected @type {Renderer} */
    this.R = parent;
    Scene.initChild(this);

    /** Scale of the object. @type {Number} */
    this.scale = 1;
    /** Brightness and shininess. @type {vec2} */
    this.specular = spec || [0.2, 2];
    /** Flags. @type {Number} */
    this.flags = flags || 0;
    /** Point size, if point cloud. @type {Number} */
    this.pointSize = 5;
    /** Texture of the Mesh. @type {Texture} */
    this.texture = (texture instanceof Texture) ? texture : new Texture(this.R, texture);

    /** @protected Triangles. @type {Uint16Array} */
    this.T = [];
    /** @protected Vertices. @type {Float32Array} */
    this.V = [];
    /** @protected Normals. @type {Float32Array} */
    this.N = [];
    /** @protected Texture coordinates. @type {Float32Array} */
    this.U = [];
    /** @protected Lines. @type {Uint16Array} */
    this.L = [];

    /** @private Buffers, with fields T, V, N, U, L. */
    this.buffer = {};
    this.buffer.T = this.R.ctx.createBuffer();
    this.buffer.V = this.R.ctx.createBuffer();
    this.buffer.N = this.R.ctx.createBuffer();
    this.buffer.U = this.R.ctx.createBuffer();
    this.buffer.L = this.R.ctx.createBuffer();

    // End of the constructor
    return this;
}


/* Settings */          /** Flag: no diffuse light on the Mesh.  @cfg @type {Number} */
Mesh.NODIFFUSE = 1;     /** Flag: no specular light on the Mesh. @cfg @type {Number} */
Mesh.NOSPECULAR = 2;    /** Flag: no ambient light on the Mesh.  @cfg @type {Number} */
Mesh.NOAMBIENT = 4;     /** Flag: wireframe display of the Mesh. @cfg @type {Number} */
Mesh.WIREFRAME = 8;     /** Flag: point cloud display the Mesh.  @cfg @type {Number} */
Mesh.POINTCLOUD = 16;


/** Overload this function to define the Mesh animation.
 *
 * This function is called once per frame.
 * It should called the {@link Texture#animate animate} function of the Mesh {@link Mesh#texture}.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Mesh.prototype.animate = function (dt) {
    'use strict';
    this.texture.animate(dt);
    return;
};

/** Draw the Mesh in its parent's Scene. @protected */
Mesh.prototype.draw = function () {
    'use strict';
    if (this.V && this.T) {
        var gl = this.R.ctx;

        // Set position and normals
        gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.V);
        gl.vertexAttribPointer(this.R.loc.aPosition, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.N);
        gl.vertexAttribPointer(this.R.loc.aNormal, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.U);
        gl.vertexAttribPointer(this.R.loc.aTexCoord, 2, gl.FLOAT, false, 0, 0);

        // Set object properties
        var f = [this.flags & Mesh.NODIFFUSE, this.flags & Mesh.NOSPECULAR,
            this.flags & Mesh.POINTCLOUD];
        gl.uniform4f(this.R.loc.uObjColor, 1, 1, 1, 1);
        gl.uniform2f(this.R.loc.uObjSpecular, this.specular[0], this.specular[1]);
        gl.uniform4i(this.R.loc.uObjFlags, !f[0], !f[1], f[2], 0);
        this.texture.toUniform(this.R.uSamplerColor, 0);

        // Set matrices
        gl.uniform1f(this.R.loc.uObjScale, 1 / this.scale);
        gl.uniformMatrix4fv(this.R.loc.uMatView, false, this.parent.matView);
        gl.uniformMatrix3fv(this.R.loc.uMatNormals, false, this.parent.matNormals);

        // Set ambient light
        if (this.flags & Mesh.NOAMBIENT) {
            gl.uniform3f(this.R.loc.uLitAmbient, 0, 0, 0);
        } else {
            var amb = this.parent.litSum;
            gl.uniform3f(this.R.loc.uLitAmbient, amb[0], amb[1], amb[2]);
        }

        // Draw elements
        gl.uniform1f(this.R.loc.uPointSize, 0);
        if (this.flags & Mesh.POINTCLOUD) {
            gl.uniform1f(this.R.loc.uPointSize, this.pointSize);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.V);
            gl.drawArrays(gl.POINTS, 0, Math.round(this.V.length / 3));
        } else if (this.flags & Mesh.WIREFRAME) {
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.buffer.L);
            gl.drawElements(gl.LINES, this.L.length, gl.UNSIGNED_SHORT, 0);
        } else {
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.buffer.T);
            gl.drawElements(gl.TRIANGLES, this.T.length, gl.UNSIGNED_SHORT, 0);
        }
    }
};

/** Set the data array.
 *
 * @param {Float32Array} V
 *  Vertex positions, of size 3n with n = number of vertices.
 * @param {Uint16Array} T
 *  Triangle indices, of size 3m with m = number of triangles.
 * @param {Float32Array} [N = null]
 *  Normal coordinates of each vertex, of size 3n.
 *  Call {@link Mesh#normalizeNormals} if the normals has not unit-length.
 * @param {Float32Array} [U = null]
 *  Texture coordinate of each vertex, of size 2n.
 * @param {Uint16Array} [L = null]
 *  Line indices for wireframe rendering, of size 2p with p = number of lines.
 */
Mesh.prototype.setData = function (V, T, N, U, L) {
    'use strict';
    var n = Math.round(V.length / 3);
    var m = Math.round(T.length / 3);

    // Set V, T
    if (V.length !== 3 * n || T.length !== 3 * m) {
        throw new Error('Invalid mesh data: vertex and triangle arrays lengths must be multiple of 3');
    }
    this.V = new Float32Array(V);
    this.T = new Uint16Array(T);
    this.updateBuffers('VT');

    // Set U
    if (!U) {
        this.U = new Float32Array(2 * n);
    } else if (U.length !== 2 * n) {
        throw new Error('Invalid mesh data: texture coordinate array length must be 2n');
    } else {
        this.U = new Float32Array(U);
    }
    this.updateBuffers('U');

    // Set N
    if (!N) {
        this.recomputeNormals();
    } else if (N.length !== 3 * n) {
        throw new Error('Invalid mesh data: normal array length must be 3n');
    } else {
        this.N = new Float32Array(N);
        this.updateBuffers('N');
    }

    // Set L
    if (!L) {
        this.recomputeLines();
    } else if (L.length !== 2 * Math.round(L.length / 2)) {
        throw new Error('Invalid mesh data: line array length must be 2p');
    } else {
        this.L = new Uint16Array(L);
        this.updateBuffers('L');
    }

    // The end
    return this;
};

/** Normalize the normals to unit lengths (on place). @static
 *
 * @param {Array} normals
 *  Array of normal vectors.
 * @return {Array}
 *  the normal array, normalized to unit lengths.
 */
Mesh.normalizeNormals = function (normals) {
    'use strict';
    var n = Math.round(normals.length / 3);
    var k, kk;
    var nx, ny, nz, nlen;
    for (k = kk = 0; k < n; k++, kk += 3) {
        nx = normals[kk];
        ny = normals[kk + 1];
        nz = normals[kk + 2];
        nlen = Math.sqrt(nx * nx + ny * ny + nz * nz);
        normals[kk] /= nlen;
        normals[kk + 1] /= nlen;
        normals[kk + 2] /= nlen;
    }
    return normals;
};


/* ***************  MESH CREATION  *************** */

/** Load the Mesh from any file.
 *
 * @param {String} file
 *  Path of the JSON file.
 * @param {Function} [callback]
 *  Callback function, called with the text content of the file.
 * @param {Boolean} [async = true]
 *  Asynchronous loading: does not wait until the object is be loaded.
 */
Mesh.prototype.loadFile = function (file, callback, async) {
    'use strict';

    // Create the request
    var request = new XMLHttpRequest();
    request.open('GET', file, async);
    if (async) {
        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status !== 200) {
                    throw new Error('Cannot load the Mesh');
                }
                callback(request.responseText);
            }
        };
    }

    // Load & return
    request.send();
    if (!async) {
        if (request.status !== 200) {
            throw new Error('Cannot load the Mesh');
        }
        callback(request.responseText);
    }
    return this;
};

/** Load the Mesh from a JSON file.
 *
 * @param {String} file
 *  Path of the JSON file.
 * @param {Function} [callback]
 *  Callback function, called with the Mesh as `this`, and the loaded JS object.
 * @param {Boolean} [async = true]
 *  Asynchronous loading: does not wait until the object is be loaded.
 */
Mesh.prototype.loadJSON = function (file, callback, async) {
    'use strict';
    var that = this;

    // Callback function
    var myCallback = function (str) {
        var o = window.JSON.parse(str);
        that.setData(o.points, o.triangles, o.normals, o.textures);
        if (callback) {
            callback.call(that, o);
        }
    };

    // Load
    this.loadFile(file, myCallback, async);
    return this;
};

/** Load the Mesh from an OFF file.
 *
 * @param {String} file
 *  Path of the OFF file.
 * @param {Function} [callback]
 *  Callback function, called with the Mesh as `this`.
 * @param {Boolean} [async = true]
 *  Asynchronous loading: does not wait until the object is be loaded.
 */
Mesh.prototype.loadOFF = function (file, callback, async) {
    'use strict';
    var that = this;

    // Callback function
    var myCallback = function (str) {
        var lines = str.match(/[^\r\n]+/g);     // non-empty lines
        if (lines[0].trim().toUpperCase() !== 'OFF') {
            throw new Error('This is not an OFF file');
        }
        // Function to read the words
        var nlines = 1;
        var getLineWords = function () {
            while (/^\s*#/.test(lines[nlines])) {
                nlines++;
            }
            return lines[nlines++].trim().split(/\s+/);
        };
        // Get dimensions
        var W = getLineWords();
        var n = parseInt(W[0], 10);     // nb of points
        var m = parseInt(W[1], 10);     // nb of faces
        var k, kk;
        // Read vertices
        var V = new Float32Array(3 * n);
        for (k = kk = 0; k < n; k++, kk += 3) {
            W = getLineWords();
            if (W.length !== 3) {
                throw new Error('Invalid OFF file: at vertex #' + k);
            }
            V[kk] = parseFloat(W[0]);
            V[kk + 1] = parseFloat(W[1]);
            V[kk + 2] = parseFloat(W[2]);
        }
        // Read triangles
        var T = [];
        var a, b, c;
        var p, q;
        for (k = kk = 0; k < m; k++) {
            W = getLineWords();
            q = parseInt(W[0], 10);     // number of vertex for this face
            if (W.length !== q + 1) {
                throw new Error('Invalid OFF file: at face #' + k);
            }
            a = parseInt(W[1], 10);
            c = parseInt(W[2], 10);
            for (p = 3; p <= q; p++, kk += 3) {
                b = c;
                c = parseInt(W[p], 10);
                T.push(a, b, c);
            }
        }
        // Create the Mesh
        that.setData(V, T);
        if (callback) {
            callback.call(that);
        }
    };

    // Load
    this.loadFile(file, myCallback, async);
    return this;
};

/** Load the Mesh from an OBJ file.
 *
 * @param {String} file
 *  Path of the OBJ file.
 * @param {Function} [callback]
 *  Callback function, called with the Mesh as `this`.
 * @param {Boolean} [async = true]
 *  Asynchronous loading: does not wait until the object is be loaded.
 */
Mesh.prototype.loadOBJ = function (file, callback, async) {
    'use strict';
    var that = this;

    // Callback function
    var myCallback = function (str) {
        var lines = str.match(/[^\r\n]+/g);     // non-empty lines

        // Function to read the words
        var nlines = 0;
        var getLineWords = function () {
            while (/^\s*#/.test(lines[nlines])) {
                nlines++;
            }
            if (nlines >= lines.length) {
                return [];
            }
            var line = lines[nlines++].trim();
            if (line.match(/^f.*\//)) {
                throw new Error('Unsupported OBJ format, no "/"" allowed: at line #' + nlines);
            }
            return line.split(/\s+/);
        };
        var W;
        var readWords = function () {
            W = getLineWords();
            return W.length;
        };

        // Read the file
        var V = [], T = [], N = [], U = [];
        var what, n;
        var a, b, c;
        var warns = {}, warnList = [];
        while (readWords()) {
            what = W[0];
            if (what === 'v') {
                if (W.length !== 4) {
                    throw new Error('Invalid OFF file: at vertex #' + Math.round(V.length / 3));
                }
                a = parseFloat(W[1]);
                b = parseFloat(W[2]);
                c = parseFloat(W[3]);
                V.push(a, b, c);
            } else if (what === 'f') {
                if (W.length !== 4) {
                    throw new Error('Invalid OFF file: at face #' + Math.round(T.length / 3));
                }
                n = Math.round(V.length / 3) + 1;
                a = parseInt(W[1], 10) - 1;
                b = parseInt(W[2], 10) - 1;
                c = parseInt(W[3], 10) - 1;
                T.push((a >= 0) ? a : a + n, (b >= 0) ? b : b + n, (c >= 0) ? c : c + n);
            } else if (what === 'vt') {
                if (W.length !== 3) {
                    throw new Error('Invalid OFF file: at texture coord. #' + Math.round(U.length / 3));
                }
                a = parseFloat(W[1]);
                b = parseFloat(W[2]);
                U.push(a, b);
            } else if (what === 'vn') {
                if (W.length !== 4) {
                    throw new Error('Invalid OFF file: at normal #' + Math.round(N.length / 3));
                }
                a = parseFloat(W[1]);
                b = parseFloat(W[2]);
                c = parseFloat(W[3]);
                N.push(a, b, c);
            } else if (!warns[what]) {
                warns[what] = true;
                warnList.push(what);
            }
        }

        // Display warnings
        if (warnList.length) {
            window.console.warn('Unsupported options in the OBJ file were ignored.\n'
                + 'Ignored keywords: ' + warnList.sort().join(', '));
        }

        // Create the Mesh
        that.setData(V, T, N.length ? Mesh.normalizeNormals(N) : null, U.length ? U : null);
        if (callback) {
            callback.call(that);
        }
    };

    // Load
    this.loadFile(file, myCallback, async);
    return this;
};

/** Generate a Cube.
 *
 * @param {Number} [a = 1]
 *  Half-width of the cube.
 * @param {Number} [b = a]
 *  Half-height of the cube.
 * @param {Number} [c = a]
 *  Half-depth of the cube.
 */
Mesh.prototype.makeCube = function (a, b, c) {
    'use strict';
    a = a || 1;
    b = b || a;
    c = c || a;
    var V = [
        a, b, +c, -a, b, +c, -a, -b, +c, a, -b, +c,         // Z = +c
        a, b, -c, -a, b, -c, -a, -b, -c, a, -b, -c,         // Z = -c
        a, +b, c, -a, +b, c, -a, +b, -c, a, +b, -c,         // Y = +b
        a, -b, c, -a, -b, c, -a, -b, -c, a, -b, -c,         // Y = -b
        +a, b, c, +a, -b, c, +a, -b, -c, +a, b, -c,         // X = +a
        -a, b, c, -a, -b, c, -a, -b, -c, -a, b, -c          // X = -a
    ];
    var N = [
        0, 0, +1, 0, 0, +1, 0, 0, +1, 0, 0, +1,
        0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1,
        0, +1, 0, 0, +1, 0, 0, +1, 0, 0, +1, 0,
        0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0,
        +1, 0, 0, +1, 0, 0, +1, 0, 0, +1, 0, 0,
        -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0
    ];
    var T = [
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7,
        8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15,
        16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23
    ];
    this.setData(V, T, N);
    return this;
};

/** Generate a sphere.
 *
 * @param {Number} [r = 1]
 *  Radius of the sphere.
 * @param {Number} [n = 16]
 *  Resolution of the sphere.
 * @param {Number} [uprct = 1]
 *  Display only a part of the sphere, form 0 (a meridian) to 1 (whole sphere).
 * @param {Number} [vprct = 1]
 *  Display only a part of the sphere, form 0 (the equator) to 1 (whole sphere).
 */
Mesh.prototype.makeSphereUV = function (r, n, uprct, vprct) {
    'use strict';
    r = r || 1;
    n = n || 16;
    uprct = uprct || 1;
    vprct = vprct || 1;
    var i, ii, j, u, v, x, y, z;

    // Initialize arrays
    var Npts = (2 * n + 1) * (n + 1);
    var T = new Uint16Array(12 * n * n);    // 4N^2 triangles
    var V = new Float32Array(3 * Npts);
    var N = new Float32Array(3 * Npts);
    var U = new Float32Array(2 * Npts);

    // Pre-compute sine and cosine
    var sinu = new Float32Array(2 * n + 1);
    var cosu = new Float32Array(2 * n + 1);
    var sinv = new Float32Array(n + 1);
    var cosv = new Float32Array(n + 1);
    for (u = 0; u <= 2 * n; u++) {
        sinu[u] = Math.sin((u / n - 1) * uprct * Math.PI);
        cosu[u] = Math.cos((u / n - 1) * uprct * Math.PI);
    }
    for (v = 0; v <= n; v++) {
        sinv[v] = +Math.cos((v / n - 1 / 2) * vprct * Math.PI); // = sin(. + pi/2)
        cosv[v] = -Math.sin((v / n - 1 / 2) * vprct * Math.PI); // = cos(. + pi/2)
    }

    // Compute values
    for (i = j = u = 0; u <= 2 * n; u++, i++) {
        for (v = 0; v < n; v++, i++) {
            y = -cosv[v];
            x = sinv[v] * sinu[u];
            z = sinv[v] * cosu[u];
            U[2 * i] = u / (2 * n);
            U[2 * i + 1] = v / n;
            N[3 * i] = x;
            N[3 * i + 1] = y;
            N[3 * i + 2] = z;
            V[3 * i] = x * r;
            V[3 * i + 1] = y * r;
            V[3 * i + 2] = z * r;
            ii = i - n - 1;
            if (ii >= 0) {
                T[6 * j] = i;
                T[6 * j + 1] = i + 1;
                T[6 * j + 2] = ii + 1;
                T[6 * j + 3] = i;
                T[6 * j + 4] = ii + 1;
                T[6 * j + 5] = ii;
                j++;
            }
        }
        y = -cosv[v];
        x = sinv[v] * sinu[u];
        z = sinv[v] * cosu[u];
        U[2 * i] = u / (2 * n);
        U[2 * i + 1] = v / n;
        N[3 * i] = x;
        N[3 * i + 1] = y;
        N[3 * i + 2] = z;
        V[3 * i] = x * r;
        V[3 * i + 1] = y * r;
        V[3 * i + 2] = z * r;
    }
    this.setData(V, T, N, U);
    return this;
};


/* ***************  PROTECTED METHODS  *************** */

/** Update the buffers. @protected
 *
 * This must be called after manual changes of Mesh arrays.
 *
 * @param {String} [which = 'TVNUL']
 *  Which buffer to update.
 *  Can contains `V` (vertices), `T` (triangles), `N` (normals),
 *      `U` (texture), `L` (lines).
 */
Mesh.prototype.updateBuffers = function (which) {
    'use strict';
    which = (which || 'VTNUL').toUpperCase();
    var gl = this.R.ctx;
    var processBuffer = function(letter, buffer, data, Type, isElmt) {
        if (which.indexOf(letter) >= 0) {
            var bufferData = (data instanceof Type) ? data : new Type(data);
            var target = isElmt ? gl.ELEMENT_ARRAY_BUFFER : gl.ARRAY_BUFFER;
            gl.bindBuffer(target, buffer);
            gl.bufferData(target, bufferData, gl.STATIC_DRAW);
        }
    };
    processBuffer('T', this.buffer.T, this.T, Uint16Array, true);
    processBuffer('V', this.buffer.V, this.V, Float32Array);
    processBuffer('N', this.buffer.N, this.N, Float32Array);
    processBuffer('U', this.buffer.U, this.U, Float32Array);
    processBuffer('L', this.buffer.L, this.L, Uint16Array, true);
    return this;
};

/** Recompute the normals of each vertex. @protected */
Mesh.prototype.recomputeNormals = function () {
    'use strict';
    var nn = this.V.length; // n = Math.round(nn / 3);     // number of points
    var mm = this.T.length, m = Math.round(mm / 3);     // number of triangles
    var k, kk;
    var normals = new Float32Array(nn);
    var ia, ib, ic;
    var xab, xac, xbc, yab, yac, ybc, zab, zac, zbc;
    var nx, ny, nz;
    var ablen, aclen, bclen, nlen;
    var wa, wb, wc;
    // For each triangle
    for (k = kk = 0; k < m; k++, kk += 3) {
        ia = 3 * this.T[kk];
        ib = 3 * this.T[kk + 1];
        ic = 3 * this.T[kk + 2];
        // Vectors: AB, AC, BC
        xab = this.V[ib] - this.V[ia];
        yab = this.V[ib + 1] - this.V[ia + 1];
        zab = this.V[ib + 2] - this.V[ia + 2];
        xac = this.V[ic] - this.V[ia];
        yac = this.V[ic + 1] - this.V[ia + 1];
        zac = this.V[ic + 2] - this.V[ia + 2];
        xbc = this.V[ic] - this.V[ib];
        ybc = this.V[ic + 1] - this.V[ib + 1];
        zbc = this.V[ic + 2] - this.V[ib + 2];
        ablen = Math.sqrt(xab * xab + yab * yab + zab * zab);
        aclen = Math.sqrt(xac * xac + yac * yac + zac * zac);
        bclen = Math.sqrt(xbc * xbc + ybc * ybc + zbc * zbc);
        // Normal and weights
        nx = yab * zac - zab * yac;
        ny = zab * xac - xab * zac;
        nz = xab * yac - yab * xac;
        nlen = Math.sqrt(nx * nx + ny * ny + nz * nz);
        wa = Math.asin(nlen / ablen / aclen) / nlen;
        normals[ia] += nx * wa;
        normals[ia + 1] += ny * wa;
        normals[ia + 2] += nz * wa;
        wb = Math.asin(nlen / ablen / bclen) / nlen;
        normals[ib] += nx * wb;
        normals[ib + 1] += ny * wb;
        normals[ib + 2] += nz * wb;
        wc = Math.asin(nlen / aclen / bclen) / nlen;
        normals[ic] += nx * wc;
        normals[ic + 1] += ny * wc;
        normals[ic + 2] += nz * wc;
    }
    this.N = Mesh.normalizeNormals(normals);
    this.updateBuffers('N');
    return this;
};

/** Recompute the line segments from triangles, for wireframe rendering. @protected */
Mesh.prototype.recomputeLines = function () {
    'use strict';
    var mat = [];
    var lines = [];
    var processLine = function (i, j) {
        var p = Math.min(i, j),
            q = Math.max(i, j);
        mat[p] = mat[p] || [];
        if (!mat[p][q]) {
            mat[p][q] = true;
            lines.push(p, q);
        }
    };
    var kk, kkmax = this.T.length;
    for (kk = 0; kk < kkmax; kk += 3) {
        processLine(this.T[kk], this.T[kk + 1]);
        processLine(this.T[kk], this.T[kk + 2]);
        processLine(this.T[kk + 1], this.T[kk + 2]);
    }
    this.L = lines;
    this.updateBuffers('L');
    return this;
};
