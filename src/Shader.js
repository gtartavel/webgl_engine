/*jslint browser: true, vars: true, plusplus: true, bitwise: true, regexp: true */
/*exported Shader */


/** A Shader program.
 *
 * This class handle a Shader:
 *
 *  + loading.
 *  + parsing to replace macros.
 *  + extracting uniform locations.
 *  + compilation.
 *
 * Note that macro of the form:
 *
 *     #define NAME default // js: expr;
 *
 * are replaced in ce source code by
 *
 *     #define NAME <value>
 *
 * where `<value>` is computed in JavaScript by:
 *
 *     value = eval(expr)
 *
 * @constructor
 *  Create/load a Shader.
 */
function Shader(R, isFrag, arg) {
    'use strict';
    /** @protected @type {Renderer} */
    this.R = R;
    /** @protected Is it a fragment (or vertex) Shader? @type {Boolean} */
    this.isFrag = isFrag;
    /** @protected Shader macros values. */
    this.macros = {};

    // Load the source
    if (!arg) {
        /** @protected Shader source code. @type {String} */
        this.source = Shader.getDefaultSource(isFrag);
    } else {
        var data = window.document.getElementById(arg) || arg;
        this.source = (data instanceof window.HTMLElement) ? Shader.loadScript(data) : arg;
    }

    // Compile the shader
    if (typeof this.source !== 'string') {
        throw new Error('Invalid shader source');
    }
    this.compile(); // initialize: this.shader
    return this;
}


/** Path to default shaders. @cfg @type {String} */
Shader.DEFAULT_PATH = './';


/* ***************  PROTECTED METHODS  *************** */

/** Compile the Shader. @protected */
Shader.prototype.compile = function () {
    'use strict';
    var gl = this.R.ctx;
    this.source = Shader.parseMacros(this.source, this.macros);
    this.shader = gl.createShader(this.isFrag ? gl.FRAGMENT_SHADER : gl.VERTEX_SHADER);
    gl.shaderSource(this.shader, this.source);
    gl.compileShader(this.shader);
    if (!gl.getShaderParameter(this.shader, gl.COMPILE_STATUS)) {
        throw new Error('Shader compilation error: ' + gl.getShaderInfoLog(this.shader));
    }
    return this;
};

/** Extract all the locations from the source code. @protected
 *
 * **Warning:** the Shader must be compiled and bound to a {@link Renderer}.
 *
 * @param [obj = {}]
 *  Object used to store the locations:
 *  shader location `foo` will be stored in `obj.foo`.
 * @return obj
 *  The object, whose fields are the location of the uniform/attributes.
 */
Shader.prototype.extractLocations = function (obj) {
    'use strict';
    obj = obj || {};
    var that = this;
    var regParts = [
        '^[ \\t]*',
        '(attribute|uniform)[ \\t]+',       // attr or unif
        '(\\w+)[ \\t]+(\\w+)[ \\t]*',       // type and name
        '(\\[[ \\t]*\\S*[ \\t]*\\])*',      // arrays
        '(;|=)'
    ];

    // Process each attribute / uniform
    var reg = new RegExp(regParts.join(''), 'gm');
    var match;
    var getMatch = function () {
        match = reg.exec(that.source);
        return Boolean(match);
    };
    var name, uLength;
    while (getMatch()) {
        name = match[3];
        uLength = (match[1] === 'uniform') ? match[4] || '' : false;
        obj[name] = this.getArraysLocations(name, uLength);
    }

    // Return the object
    return obj;
};


/* **************  PRIVATE METHODS  *************** */

/** Load the content of a file. @static @private
 *
 * @param {String} path
 *  Path of the file to be loaded.
 * @param {Function} [callback = null]
 *  If provided, loading is asynchronous.
 *  Callback is called with laoded text.
 *
 * @return {String | null}
 *  If no callback, loading  is synchronous.
 *  Loaded text is returned.
 */
Shader.loadFile = function (path, callback) {
    'use strict';
    var req = new XMLHttpRequest();
    req.open('GET', path, false);
    if (!callback) {
        req.send(null);
        if (req.status !== 200) {
            throw new Error('Cannot load the shader from the given file');
        }
        return req.responseText;
    }
    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            if (req.status !== 200) {
                throw new Error('Cannot load the shader from the given file');
            }
            callback(req.responseText);
        }
    };
    req.send(null);
};

/** Load the content of a SCRIPT element. @static @private
 *
 * @param {String | HTMLElement} elmt
 *  A script element or its ID.
 *
 * @return {String}
 *  The content of the script element.
 */
Shader.loadScript = function (elmt) {
    'use strict';
    var script = (typeof elmt !== 'string') ? elmt : window.document.getElementById(elmt);
    if (!script) {
        throw new Error('Invalid script element');
    }

    // Get the source code
    var child, code = '';
    for (child = elmt.firstChild; child; child = child.nextSigling) {
        if (child.nodeType === 3) {
            code += child.textContent;
        }
    }

    // Compile it
    return code;
};

/** Load the default Shader source, either from SCRIPT or .glsl file. @static @private
 *
 * @param {Boolean} isFrag
 *  Load the Vertex (`false`) or Fragment (`true`) Shader script.
 *
 * @return {String}
 *  Default source code of the Shader.
 */
Shader.getDefaultSource = function (isFrag) {
    'use strict';
    var source;
    var path = Shader.DEFAULT_PATH;
    path += (path.slice(-1) === '/') ? '' : '/';
    path += isFrag ? 'fragment.glsl' : 'vertex.glsl';
    try {
        source = Shader.loadScript(Shader.getDefaultScript(isFrag));
    } catch (e) {
        try {
            source = Shader.loadFile(path);
        } catch (ee) {
            throw new Error('No default shader available.');
        }
    }
    return source;
};

/** Get default SCRIPT element for the Shader. @static @private
 *
 * @param {Boolean} isFrag
 *  Load the Vertex (`false`) or Fragment (`true`) Shader script.
 *
 * @return {HTMLElement}
 *  The default script element.
 */
Shader.getDefaultScript = function (isFrag) {
    'use strict';
    var typeStr = isFrag ? 'frag' : 'vert';
    var strSearch = function (source, searched) {
        return source.indexOf(searched) >= 0;
    };
    var scripts = window.document.getElementsByTagName('script');
    var k, type, elmts = [];
    for (k = 0; k < scripts.length; k++) {
        type = scripts[k].type.toLowerCase();
        if (strSearch(type, 'shader') && strSearch(type, typeStr)) {
            elmts.push(scripts[k]);
        }
    }
    if (elmts.length !== 1) {
        throw new Error('Cannot find shader script');
    }
    return elmts.pop();
};

/** Parse the macros in the source code, and evaluate JS comments. @static @private
 *
 * See the {@link Shader} class documentation for parsed macro structures.
 *
 * @param {String} source
 *  Source code to be parsed.
 * @param obj
 *  Object used to store the macros:
 *  shader macro `FOO` will be stored in `obj.FOO`.
 * @return {String}
 *  Source code, with evaluated JS macros.
 */
Shader.parseMacros = function (source, obj) {
    'use strict';
    var s = source;
    var evaluate = eval;
    var regParts = [
        '^([ \\t]*)#define[ \\t]+',     // #define
        '(\\S+)[ \\t]+(\\S+)[ \\t]*',   // NAME <value>
        '(?:\\/(?:\\/|\\*)[ \\t]*',     // <comment start>
        '(?:js|JS):([^;]+);',           // js: <cmd>
        '(?:[ \\t]*\\*\\/)?)?'          // <comment end ?>
    ];

    // Process each match
    var reg = new RegExp(regParts.join(''), 'gm');
    var match;
    var getMatch = function () {
        match = reg.exec(source);
        return Boolean(match);
    };
    var name, value;
    var what, by;
    while (getMatch()) {
        name = match[2];
        value = parseFloat(match[3]);
        value = isNaN(value) ? match[3] : value;

        // Evaluate the JS
        if (match[4]) {
            try {
                value = evaluate(match[4]);
            } catch (e) {
                throw new Error('In Shader, JS eval failed: ' + e.message);
            }
            what = regParts[0] + name + '[ \t].*$';
            by = '$1#define ' + name + ' ' + value;
            s = s.replace(new RegExp(what, 'gm'), by);
        }

        // Store the macro into the object
        obj[name] = value;
    }

    // Return the new source
    return s;
};

/** Get the locations of an uniform array. @private
 *
 * Works also to
 *
 *  + get the location of an uniform variable.
 *  + get and activate an attribute array.
 *
 * The Shader must be compiled and bound to a {@link Renderer}.
 *
 * @param {String} name
 *  Name of the attribute / uniform.
 * @param {String} [uStrLength = false]
 *  Length of the uniform array.
 *  Set to the empty string `''` for uniform scalars, and to `false` for an attribute.
 * @return
 *  An array of location.
 *  In case of an uniform or attribute, return its location.
 */
Shader.prototype.getArraysLocations = function (name, uStrLength) {
    'use strict';
    var gl = this.R.ctx;
    var prog = this.R.program;

    // Get and enable attributes
    if (typeof uStrLength !== 'string') {
        var att = gl.getAttribLocation(prog, name);
        gl.enableVertexAttribArray(att);
        return att;
    }

    // Get the array length or the uniform
    var reg = /^[ \t]*\[[ \t]*(\S+)[ \t]*\](.*)$/;
    var match = reg.exec(uStrLength || '');
    if (!match) {
        return gl.getUniformLocation(prog, name);
    }

    // Build the array
    var N = parseFloat(match[1]);
    N = (isNaN(N)) ? this.macros[match[1]] : N;
    if (!N) {
        throw new Error('Cannot parse length value: ' + match[1]);
    }
    var k, suffix, out = [];
    for (k = 0; k < N; k++) {
        suffix = '[' + k + ']';
        out[k] = this.getArraysLocations(name + suffix, match[2]);
    }
    return out;
};
