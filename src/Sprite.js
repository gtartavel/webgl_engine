/*jslint browser: true, vars: true, plusplus: true, bitwise: true */
/*global Texture, Mesh, Float32Array, mat3, mat4 */
/*exported Sprite */


/** A 2D Sprite, to display images, textures, or text on the screen.
 *
 * A sprite is a quadrilateral object with a texture in it.
 * It is useful to display informations or images on the 2D screen (e.g. HUD interface).
 *
 * @constructor Create a Sprite.
 *
 * @param {Renderer} R
 *  Parent Renderer.
 * @param {Texture | String | Array | Number} texture
 *  Texture, Image URL, or color (gray / RGB / RGBA).
 * @param {Number} [alpha = 1]
 *  Opacity of the Sprite.
 */
function Sprite(R, texture, alpha) {
    'use strict';
    /** @protected @type {Renderer} */
    this.R = R;
    /** Texture of the Sprite. @type {Texture} */
    this.texture = (texture instanceof Texture) ? texture : new Texture(this.R, texture);

    /** @protected Z position. @type {Number}. */
    this.z = 0;
    /** Opacity. @type {Number} */
    this.alpha = (alpha !== undefined && alpha !== null) ? alpha : 1;

    /** @private Buffers, with field V. */
    this.buffer = {'V': this.R.ctx.createBuffer()};
    this.setPosition([0, 0, 1, 1]);

    // Append it and return
    this.R.sprites.push(this);
    return this;
}


/** Set the position of the Sprite.
 *
 * @param {Array} [positions = null]
 *  Position array, either:
 *
 *  + 4 values: `Xmin`, `Ymin`, `Xmax`, `Ymax`.
 *  + 8 values: `X,Y` of bottom-left, bottom-right, top-right, top-left corners.
 *  + 12 values: `X,Y,Z` of the corners (same order as above).
 * @param  {Number} [z = null]
 *  `Z` position of the Sprite, between 0 (front) and 1 (back).
 */
Sprite.prototype.setPosition = function (pos, z) {
    'use strict';
    pos = pos || this.V || [];
    z = (z !== undefined && z !== null) ? z : this.z;
    this.z = z;

    // Format position array
    if (pos.length === 4) {
        pos = [pos[0], pos[1], z, pos[2], pos[1], z, pos[2], pos[3], z, pos[0], pos[3], z];
    } else if (pos.length === 8) {
        pos = [pos[0], pos[1], z, pos[2], pos[3], z, pos[4], pos[5], z, pos[6], pos[7], z];
    } else if (pos.length !== 12) {
        throw new Error('Invalid position array');
    }

    // Set to buffer
    var gl = this.R.ctx;
    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.V);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pos), gl.STATIC_DRAW);
    return this;
};

/** Overload this function to define the Sprite animation.
 *
 * This function is called once per frame.
 * It should called the {@link Texture#animate animate} function of the Sprite {@link Sprite#texture}.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Sprite.prototype.animate = function (dt) {
    'use strict';
    this.texture.animate(dt);
    return;
};

/** Draw the Sprite. @protected
 *
 * Note: {@link Sprite#drawInit} must be called once first before drawing any Sprite.
 */
Sprite.prototype.draw = function () {
    'use strict';
    var gl = this.R.ctx;
    this.texture.toUniform(this.R.uSamplerColor, 0);

    // Draw it
    gl.uniform4f(this.R.loc.uObjColor, 1, 1, 1, this.alpha);
    gl.bindBuffer(gl.ARRAY_BUFFER, (this.buffer || Sprite.defaultMesh.buffer).V);
    gl.vertexAttribPointer(this.R.loc.aPosition, 3, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, Sprite.defaultMesh.buffer.T);
    gl.drawElements(gl.TRIANGLES, Sprite.defaultMesh.T.length, gl.UNSIGNED_SHORT, 0);
};

/** Prepare for drawing Sprites. @static @protected
 *
 * @param {Renderer} R
 *  Renderer to be initialized.
 */
Sprite.drawInit = function (R) {
    'use strict';
    var gl = R.ctx;
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE); // ONE or ONE_MINUS_SRC_ALPHA

    // Get buffers
    if (!Sprite.defaultMesh) {
        var scene = R.scene;
        Sprite.defaultMesh = new Mesh(R);
        R.scene = scene;
        Sprite.defaultMesh.T = [0, 1, 2, 0, 2, 3];
        Sprite.defaultMesh.N = [0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1];
        Sprite.defaultMesh.V = [0, 0, +0, 1, 0, +0, 1, 1, +0, 0, 1, +0];
        Sprite.defaultMesh.U = [0, 0, 1, 0, 1, 1, 0, 1];
        Sprite.defaultMesh.updateBuffers('TNVU');
    }

    // Set buffers
    gl.bindBuffer(gl.ARRAY_BUFFER, Sprite.defaultMesh.buffer.N);
    gl.vertexAttribPointer(R.loc.aNormal, 3, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ARRAY_BUFFER, Sprite.defaultMesh.buffer.U);
    gl.vertexAttribPointer(R.loc.aTexCoord, 2, gl.FLOAT, false, 0, 0);

    // Get matrices
    Sprite.matProj = Sprite.matProj || mat4.ortho(mat4.create(), 0, 1, 0, 1, 0, 1);
    Sprite.matView = Sprite.matView || mat4.create();
    Sprite.matNormals = Sprite.matNormals || mat3.create();

    // Set matrices
    gl.uniform3f(R.loc.uFogRange, 0, 1, 0);
    gl.uniformMatrix4fv(R.loc.uMatProj, false, Sprite.matProj);
    gl.uniformMatrix4fv(R.loc.uMatView, false, Sprite.matView);
    gl.uniformMatrix3fv(R.loc.uMatNormals, false, Sprite.matNormals);

    // Set object properties
    gl.uniform3f(R.loc.uLitAmbient, 1, 1, 1);
    gl.uniform4i(R.loc.uObjFlags, 0, 0, 0, 0);
    gl.uniform1f(R.loc.uObjScale, 1);
    gl.uniform1f(R.loc.uPointSize, 0);
};
