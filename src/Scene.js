/*jslint browser: true, vars: true, plusplus: true */
/*global Renderer, Light, vec3, mat3, mat4 */
/*exported Scene */

/** A 3D scene.
 *
 * A Scene handles:
 *
 *  + any number of {@link Mesh}es.
 *  + any number of sub-{@link Scene}s.
 *  + any number of {@link Light}s.
 *
 * @constructor
 *  Create a new Scene.
 *
 * @param {Scene | Renderer} parent
 *  The parent Scene or Renderer.
 */
function Scene(parent) {
    'use strict';
    /** @protected Lights of the Scene. @type {Light[]} */
    this.lights = [];
    /** @protected Children Scenes and Meshes. @type {Array} */
    this.children = [];
    /** @protected @type {Scene} */
    this.parent = parent;
    /** @protected @type {Renderer} */
    this.R = parent;
    Scene.initChild(this);

    /** Transformation matrix. @type {mat4} */
    this.matrix = null;
    /** @protected Precomputed global matrix. @type {mat4} */
    this.matView = mat4.create();
    /** @protected Precomputed normal matrix. @type {mat3} */
    this.matNormals = mat3.create();
    /** Ambient light in the Scene. @type {vec4} */
    this.litAmbient = null;
    /** @protected Precomputed total ambient light. @type {vec3} */
    this.litSum = vec3.create();
    return this;
}


/** Precompute the transformation matrices and the ambient light. @protected */
Scene.prototype.precompute = function () {
    'use strict';

    // Matrices
    var parentMatrix = (this.parent || this.R.camera).matView;
    if (this.matrix) {
        mat4.mul(this.matView, parentMatrix, this.matrix);
    } else {
        mat4.copy(this.matView, parentMatrix);
    }
    mat3.normalFromMat4(this.matNormals, this.matView);

    // Ambient light
    var parentLitSum = (this.parent || this.R).litSum;
    if (this.litAmbient) {
        vec3.scaleAndAdd(this.litSum, parentLitSum, this.litAmbient, this.litAmbient[3]);
    } else {
        vec3.copy(this.litSum, parentLitSum);
    }

    // Recursively
    var k;
    for (k = 0; k < this.children.length; k++) {
        if (this.children[k].precompute) {
            this.children[k].precompute();
        }
    }
};

/** When overloading {@link Scene#animate}, call this function to propagate the animation.
 *
 * This function animate all the children (Scenes, Meshes, Lights) of the Scene.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Scene.prototype.animateChildrenAndLights = function (dt) {
    'use strict';
    var k;
    for (k = 0; k < this.lights.length; k++) {
        this.lights[k].animate(dt);
    }
    for (k = 0; k < this.children.length; k++) {
        this.children[k].animate(dt);
    }
};

/** Overload this function to define the Scene animation.
 *
 * This function is called once per frame.
 * It should called the {@link Scene#animateChildrenAndLights} function.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Scene.prototype.animate = function (dt) {
    'use strict';
    this.animateChildrenAndLights(dt);
};

/** Draw the Scene. @protected
 *
 * @param litCount
 *  Current number of lights.
 */
Scene.prototype.draw = function (litCount) {
    'use strict';
    var k;

    // Add the lights
    for (k = 0; k < this.lights.length; k++) {
        this.lights[k].toUniform(litCount++);
    }

    // Draw the scene content
    for (k = 0; k < this.children.length; k++) {
        this.children[k].draw(litCount);
    }

    // Remove the lights
    for (k = 0; k < this.lights.length; k++) {
        Light.unsetUniform(this.R, litCount--);
    }
};


/* ***************  PROTECTED METHODS  *************** */

/** @static @private */
Scene.initChild = function (child) {
    'use strict';
    var parent = child.parent;
    if (parent instanceof Renderer) {
        child.parent = null;
        child.R = parent;
        child.R.scene = child;
    } else if (parent instanceof Scene) {
        child.R = parent.R;
        parent.children.push(child);
    } else {
        throw new Error('Invalid parent');
    }
};
