/*jslint browser: true, vars: true, plusplus: true */
/*global vec3, mat4 */
/*exported Camera */

/** A 3D camera.
 *
 *  The Camera handles options related to the Scene capture:
 *  point of view, resolution, etc.
 *
 *  This class deals with:
 *
 *  + camera location and orientation.
 *  + projection type (perspective or ortho), FOV and ratio.
 *  + distance bounds and fog.
 *
 * @constructor
 *  Create a Camera located in 0 and facing the Z direction.
 *
 * @param {Boolean} [usePerspective = false]
 *  Same as in {@link Camera#setPerspective}.
 */
function Camera(usePerspective) {
    'use strict';

    // Camera orientation properties
    this.position = [0, 0, 0];
    this.yaw = 0;
    this.pitch = 0;
    this.roll = 0;  // TODO: use direction instead?

    // Camera projection properties
    this.vfov = usePerspective ? 70 * Math.PI / 180 : null;
    this.vrange = usePerspective ? null : 2;
    this.ratio = 16 / 9;
    this.dmin = 0.1;
    this.dmax = 10;


    /** Fog starting distance. @type {Number} */
    this.dfog = 5;
    /** Fog thickening rate. @type {Number} */
    this.fogExp = 2;

    // Buffers
    this.matView = mat4.create();
    this.matProj = mat4.create();
    this.updateMatProj();

    // End of constructor
    return this;
}


/** Extract the Camera properties to uniform variable of the shader. @protected
 *
 * @param {Renderer} R
 */
Camera.prototype.toUniform = function (R) {
    'use strict';
    this.setRatio(R.canvas);
    R.ctx.uniform3f(R.loc.uFogRange, this.dfog, this.dmax, this.fogExp);
    R.ctx.uniformMatrix4fv(R.loc.uMatProj, false, this.matProj);
};

/** Overload this function to define the Camera animation.
 *
 * This function is called once per frame.
 *
 * @param {Number} dt
 *  Elapsed time, in ms, since last animation.
 */
Camera.prototype.animate = function () {
    'use strict';
    return;
};


/* ***************  CAMERA SETTERS  *************** */

/** Change the location of the camera.
 *
 * @param {vec3} position
 *  Move the camera to the given position.
 */
Camera.prototype.setPosition = function (position) {
    'use strict';
    if (!position || position.length !== 3) {
        throw new Error('Invalid camera position');
    }
    this.position = position;
    this.updateMatView(); // TODO: could be optimized
    return this;
};

/** Change the orientation angles (yaw/pitch/roll) of the camera.
 *
 * @param {Number} yaw
 *  Yaw angle (to look left/right), in radians.
 * @param {Number} pitch
 *  Pitch angle (to look up/down), in radians.
 * @param {Number} [roll]
 *  Roll angle (rotate the image), in radians.
 */
Camera.prototype.setAngles = function (yaw, pitch, roll) {
    'use strict';
    this.yaw = yaw;
    this.pitch = pitch;
    if (this.roll !== undefined) {
        this.roll = roll;
    }
    this.updateMatView();
    return this;
};

/** Change the ratio of the camera.
 *
 * @param {Number | HTMLElement} arg
 *  The ratio "width over height", or a canvas to get the ratio from.
 */
Camera.prototype.setRatio = function (arg) {
    'use strict';
    var ratio = arg;
    if (arg.width && arg.height) {
        ratio = arg.width / arg.height;
    }
    this.ratio = ratio;
    this.updateMatProj();
    return this;
};

/** Change the depth range of the Camera.
 *
 * @param {Number} dmin
 *  Minimum depth the camera can get.
 * @param {Number} dmax
 *  Maximum depth the camera can get.
 * @param {Number} [dfog = dmax / 2]
 *  Depth from which fog appears.
 */
Camera.prototype.setDepthRange = function (dmin, dmax, dfog) {
    'use strict';
    this.dmin = dmin;
    this.dmax = dmax;
    this.dfog = (dfog !== undefined) ? dfog : dmax / 2;
    this.updateMatProj();
    return this;
};

/** Set the perspective type and FOV.
 *
 * @param {Boolean} usePerspective
 *  Use a perspective (true) or orthogonal (false) projection.
 * @param {Number} vfov
 *  Vertical field of view, in radians (if perspective) or in units (if ortho).
 */
Camera.prototype.setPerspective = function (usePerspective, vfov) {
    'use strict';
    this.vfov = this.vmax = 0;
    if (usePerspective) {
        this.vfov = vfov;
    } else {
        this.vrange = vfov;
    }
    this.updateMatProj();
    return this;
};


/* ***************  PRIVATE METHODS  *************** */

/** @private */
Camera.prototype.updateMatView = function () {
    'use strict';
    var t = vec3.scale([], this.position, -1);
    mat4.identity(this.matView);
    mat4.translate(this.matView, this.matView, t);
    mat4.rotateX(this.matView, this.matView, -this.yaw);
    mat4.rotateY(this.matView, this.matView, -this.pitch);
    if (this.roll) {
        mat4.rotateZ(this.matView, this.matView, -this.roll);
    }
};

/** @private */
Camera.prototype.updateMatProj = function () {
    'use strict';
    if (this.vfov) {
        mat4.perspective(this.matProj, this.vfov, this.ratio, this.dmin, this.dmax);
    } else {
        var y = this.vrange / 2;
        var x = y * this.ratio;
        mat4.ortho(this.matProj, -x, x, -y, y, this.dmin, this.dmax);
    }
};
