uniform float uObjScale;
uniform float uPointSize;

uniform mat4 uMatProj;      // Camera -> Screen
uniform mat4 uMatView;      // Mesh -> Camera
uniform mat3 uMatNormals;

attribute vec3 aPosition;   // From the Mesh viewpoint
attribute vec3 aNormal;
attribute vec2 aTexCoord;

varying vec4 vPosition;     // From the Camera viewpoint
varying vec3 vNormal;
varying vec2 vTexCoord;

// Transform points and normals
void main(void) {
    vPosition = uMatView * vec4(aPosition, uObjScale);
    vNormal = uMatNormals * aNormal;
    vTexCoord = aTexCoord;
    gl_Position = uMatProj * vPosition;
    gl_PointSize = uPointSize;
}