/** @class vec2     Vector of 2 elements -- from gl-matrix library. */
/** @class vec3     Vector of 3 elements -- from gl-matrix library. */
/** @class vec4     Vector of 4 elements -- from gl-matrix library. */
/** @class mat3     Matrix of size 3x3 -- from gl-matrix library. */
/** @class mat4     Matrix of size 4x4 -- from gl-matrix library. */
/** @class Float32Array     Typed array of floats. */
/** @class Uint16Array      Typed array of unsigned integers. */